from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from my_rest.serializers import  MySerializer
from author.models import Author
from rest_framework import mixins
from rest_framework import generics

'''
class AuthorListApi(APIView):

    def get(self, request,):
        a = Author.objects.all()
        res = MySerializer(a, many = True)
        return Response(res.data)'''

class AuthorListApi(mixins.ListModelMixin,
                  mixins.CreateModelMixin,
                  generics.GenericAPIView):

    """
    Returns a list of all authors,  and informations about them.
    

    """

    queryset = Author.objects.all()
    serializer_class = MySerializer

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    # def metadata(self, request):
    #     """
    #     Don't include the view description in OPTIONS responses.
    #     """
    #     data = super(AuthorListApi, self).metadata(request)
    #     data.pop('description')
    #     return data



class AuthorIdApi(APIView):
    """

    Returns informations about author and  ability change data.

    For more details about all authors [see here][ref].

    [ref]: http://127.0.0.1:8000/api/list/
    """

    def get(self, request, **kwargs):
        search = Author.objects.filter(id=kwargs['pk'])
        
        rez = MySerializer(search, many = True)
        return Response(rez.data)

    def post(self, request, *args, **kwargs):

        rez = MySerializer(data=request.data)
        if rez.is_valid():
            rez.save()
            return Response(rez.data)
        return Response(rez.errors)




