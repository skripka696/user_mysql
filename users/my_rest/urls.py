"""main URL Configuration

"""
from django.conf.urls import include, url
from django.contrib import admin

from author import views
#from author.views import get_author_filter
from django.conf import settings
from my_rest import views



urlpatterns = [
	
	#url(r'^home/', views.HomeView.as_view()),
	#url(r'^list/$', views.AuthorListApi.as_view()),
	url(r'^list/(?P<pk>\d+)/$', views.AuthorIdApi.as_view(), name= 'author_id'),
	url(r'^list/$', views.AuthorListApi.as_view())
]