"""main URL Configuration

"""
from django.conf.urls import include, url
from django.contrib import admin

from author import views
#from author.views import get_author_filter
from django.conf import settings
from django.conf.urls.static import static



urlpatterns = [
	
	#url(r'^home/', views.HomeView.as_view()),
	url(r'^$', views.IndexView.as_view()),
	url(r'^regist/', views.Regist.as_view(), name = 'regist'),
    url(r'^login/$',  views.Login.as_view(), name = 'login'),
    url(r'^logout/$',  views.LogOut.as_view()),
    #url(r'^get_author/$', get_author_filter),
	url(r'^get_author/$', views.AuthorView.as_view(), name = 'get_author'),
	url(r'^get_author/(?P<pk>\d+)/$', views.GetAuthorView.as_view(), name= 'get_view'),
	url(r'^get_list/$',views.GetAuthorList.as_view(), name = 'get_list'),
	url(r'^get_save/$', views.SaveView.as_view(), name = 'get_save'),

	url(r'^thanks/', views.ThanksView.as_view(), name = 'thanks'),
	url(r'^avatar/$', views.Avatar.as_view(), name = 'avatar'),
	url(r'^set_timezone/$', views.set_timezone, name = 'set_timezone'),

]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)